Este bootloader USB HID es la modificacion del bootloader de Microchip que viene en la libreria Microchip, esta modificada para trabajar en un PIC18F4550 con un crystal de 4Mhz y con la tarjeta de entrenamiento del cual se adjunta el esquematico

Ir a \Firmware - PIC18 Non-J\MPLAB.X ahi se encuentra el archivo .hex con el nombre

"MPLAB.X.production.hex" este archivo es el que se debe cargar en el PIC

El programa necesario para cargar los programas al PIC por medio del bootloader USB se puede encontrar en: Microchip Libraries for Applications 
[Microchip Libraries][]

[Microchip Libraries]:http://www.microchip.com/stellent/idcplg?IdcService=SS_GET_PAGE&nodeId=2680&dDocName=en547784

Armar el circuito con los botones de reset y boot de acuerdo al esquema que se encuentra en la carpeta esquematico hecho en proteus 7.10


-------- CODIGO EJEMPLO ----------

El en archivo bootloader_chip.h se encuentra el codigo que se debe usar en todos los programas para que no se sobreescriba el bootloader al momento de cargar un programa en el microcontrolador

Ademas se adjunta un programa hecho en el compilador CCS llamado "ejemplo.c" que enciende leds y muestra la estructura de un programa basico


Saludos 

Sabas

Colaboracion en el esquematico del M.C. Francisco Guevara

[Tecnologicobj12][]

[Tecnologicobj12]:http://tecnologicobj12.blogspot.mx/
