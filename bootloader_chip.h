//*******************************************************************************
//Header de utlidad si para que funciones correctamente en CSS el bootloader de
//Microchip
//Fecha: 11 de Octubre del 2012
//Andres Eduardo Sabas Jimenez
//Solo se debe agregar al programa pricipal
//el #include "bootloader_chip.h" y el arhivo estar en la misma carpeta del proyecto
//o #include <bootloader_chip.h> si se agrega a los headers principales del programa
// en C:\Program Files\PICC\Drivers
//*******************************************************************************

//-----------------Remapeo de vectores de interrupcion por el bootloader
#build (reset=0x1000, interrupt=0x1008)
//------------------Proteccion del codigo del bootloader
#org 0x000, 0xFFF
void bootloader(void){} //Funcion para mandar llamar el modo en bootloader
