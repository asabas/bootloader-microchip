//*******************************************************************************
//---------------------Programa de Muestra de uso basico de bootloader USB------
//------------------------- de Microchip serie 18F-----------------------------
//-------------------------10 de Octubre del 2012------------------------------
//---------------Programacion: Andres Eduardo Sabas Jimenez--------------------
//-----------------------Objetivo: Lograr el arranque de un programa------------ 
//-------------------de encender y apagar un led en un PIC con el---------------
//--------------------------------Bootloader cargado----------------------------
//*******************************************************************************

#include <18F4550.h>
#device adc=16

#FUSES HSPLL, NOWDT, NOPROTECT, BROWNOUT, NOPUT, NOCPD, NOLVP, LPT1OSC, NOMCLR, NOPBADEN, USBDIV, PLL3, CPUDIV1, VREGEN 
#use delay(clock=48000000)
#include "bootloader_chip.h" //Header para proteger el codigo del bootloader

//----------------Configuracion del micro-------------------------

void main()
{
   setup_timer_3(T3_DISABLED | T3_DIV_BY_1);

   //Enciendo y apago un led y cambio su tiempo de apagado y encendido para ver que el boot esta funcionando
   while(TRUE)
   {
      output_toggle(PIN_C0);
      delay_ms(25);  //Con solo variar el tiempo se podran dar cuenta del cambio en el programa al reprogramar facilmente con el bootloader
   }

}
